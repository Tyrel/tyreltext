#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>

#ifdef _WIN32  //windows includes
       #include<windows.h>
#endif //end windows includes

#include "room.cpp"
#include "monsters.cpp"
#include "secrets.cpp"
#include "exits.cpp"
#include "items.cpp"
using namespace std;


//VARIABLES

const int NORTH = 8;
const int SOUTH = 4;
const int EAST = 2;
const int WEST = 1;

const int OPEN = 1;
const int CLOSE = 2;

bool gameRun = true;
string PROMPT;
string prompt;
int currentX,currentY;
vector<vector<Room> > room(5, vector<Room>(5)); 
// END VARIABLES


//FUNCTIONS
void cls();
void move(int dir);
void checkMove(int dir);
void showMap();
void checkPrompt(string pmp);
void showHelp();
void initialize();
void ShowExits();
void jumpTo(int x,int y);
void openCloseExit(int dir, int oc);
//END FUNCTIONS





int main(int argc, char *argv[])
{
    initialize();
    
    while(gameRun){               
       cout<<PROMPT; //show prompt
       getline(cin,prompt); //get what the prompt is
       checkPrompt(prompt); //check the options
    } //end game loop
    
    return EXIT_SUCCESS;
}

void move(int dir){
 //MOVE FIRST
     checkMove(dir);
     //check anything after 
     if(room[currentX][currentY].IsEnd())
     {            
        gameRun=false;
        cout<<"\a";                
    }
 //check for monsters
 //check for secrets
 //check for items
}
void checkMove(int dir){    
     switch (dir){
            case 8:
                 if((room[currentX][currentY].Exits() & NORTH) == NORTH)   // if there are exits to the DIRECTION,
                 {
                   room[currentX][currentY].leaveRoom();  //LEAVE THE CURRENT ROOM
                   currentY=currentY-1;   //MOVE THE CURRENT POSITION 
                   room[currentX][currentY].enterRoom(); //ENTER NEW ROOM
                  
                   showMap();
                   cout << "You move North.\n";
                 }  else {
                    showMap();
                    cout<<"You cannot move that direction.\n";
                 }
                 break;
            case 4:
                 if((room[currentX][currentY].Exits() & SOUTH) == SOUTH)
                 {
                   room[currentX][currentY].leaveRoom();
                   currentY=currentY+1;
                   room[currentX][currentY].enterRoom();
                   
                   showMap();
                   cout << "You move South.\n";
                 }   else {
                    showMap();    
                    cout<<"You cannot move that direction.\n";
                 }
                 break;
            case 2:
                 if((room[currentX][currentY].Exits() & EAST)== EAST)
                 {
                   room[currentX][currentY].leaveRoom();
                   currentX=currentX+1;
                   room[currentX][currentY].enterRoom();
                   showMap();
                   cout << "You move East.\n";
                 } else {
                    showMap();
                    cout<<"You cannot move that direction.\n";
                 }  
                 break;
            case 1:
                 if((room[currentX][currentY].Exits() & WEST)== WEST)
                 {
                   room[currentX][currentY].leaveRoom();
                   currentX=currentX-1;
                   room[currentX][currentY].enterRoom();
                   
                   showMap();
                   cout << "You move West.\n";
                   
                 }   else {
                    showMap();
                    cout<<"You cannot move that direction.\n";
                 }
                 break;  
      }
}

void jumpTo(int x,int y){
     room[currentX][currentY].leaveRoom();
     currentX = x;
     currentY = y;
     room[currentX][currentY].enterRoom();   
}

void checkPrompt(string pmp){
     //directions
     
     
     if((pmp=="go north") || (pmp=="north")){move(NORTH);return;}
     if((pmp=="go south") || (pmp=="south")){move(SOUTH);return;}
     if((pmp=="go east") || (pmp=="east")){move(EAST);  return;}
     if((pmp=="go west") || (pmp=="west")){move(WEST);  return;}
     
     if((pmp=="close north") || (pmp=="close n")){openCloseExit(NORTH,CLOSE);    return;}
     if((pmp=="close south") || (pmp=="close s")){openCloseExit(SOUTH,CLOSE);    return;}
     if((pmp=="close east")  || (pmp=="close e")){openCloseExit(EAST,CLOSE);    return;}
     if((pmp=="close west")  || (pmp=="close w")){openCloseExit(WEST,CLOSE);    return;}
     if((pmp=="open north") || (pmp=="open n")){openCloseExit(NORTH,OPEN);       return;}
     if((pmp=="open south") || (pmp=="open s")){openCloseExit(SOUTH,OPEN);       return;}
     if((pmp=="open east")  || (pmp=="open e")){openCloseExit(EAST,OPEN);       return;}
     if((pmp=="open west")  || (pmp=="open w")){openCloseExit(WEST,OPEN);       return;}
     
     
     if((pmp=="map")||(pmp=="m")){showMap();                                                         return;}    
     if((pmp=="help") || (pmp=="?")){showHelp();                                                      return;}
     if((pmp=="look") || (pmp=="l")){ShowExits();                                                     return;}
     if((pmp=="quit") || (pmp=="exit") || (pmp=="q")){gameRun=false; return;}
     
     cls();
     cout << "What was that?";
}
void ShowExits(){
     if(room[currentX][currentY].Exits() >0){
               cout<<"There are exits to the...";
               if((room[currentX][currentY].Exits() & NORTH) == NORTH){cout<<"\nnorth"; }
               if((room[currentX][currentY].Exits() & SOUTH) == SOUTH){cout<<"\nsouth"; }     
               if((room[currentX][currentY].Exits() & EAST) == EAST){cout<<"\neast"; }     
               if((room[currentX][currentY].Exits() & WEST) == WEST){cout<<"\nwest"; }
               cout<<"\n";         
     } else {
               cout << "All of the doors are closed.";       
     }
}
          
   
void openCloseExit(int dir, int oc){
      
    int state = room[currentX][currentY].opencloseExit(dir,oc);
    // cout << state;
     
     switch (oc)
     {
      case 1:   
                 switch (state){
                 case 1:
                     cout <<"Something clicks...";                                               
                     break;
                 case 2:
                    cout <<"Dude... that's a wall.";      
                    break;
                 case 3:
                   cout <<"Nothing Happened.";       
                   break;
                 }
         break;
     case 2:
                 switch (state){
                 case 1:
                     cout <<"...Thud...";                                               
                     break;
                 case 2:
                    cout <<"Dude... that's a wall.";      
                    break;
                 case 3:
                   cout <<"Nothing Happened.";       
                   break;
                 }
         break;
    }     
}       
          
          
          
          
          
          
          
void showHelp(){
     cls();
     cout<<"Welcome Tyrel's Game\n"
         <<"\t-To Begin, Try going in a direction\n"
         <<"\t\t\"north\",\"south\",\"east\" or \"west\"\n"
         <<"\t-There is also a \"map\"\n"
         <<"\tIf you are lost, try \"look\"";
     
}

void showMap(){
    cls();
    for(int y=0; y<5;y++){
        for (int x = 0; x <5;x++){
            if(room[x][y].InRoom()){
                 cout<< "(X)\t";
            } else {
                 cout << "(.)\t";//room[x][y].Exits() << "\t";        
            }
        }   
        cout << "\n\n";
    }
}



//this will clear the screen
void cls()
{
    #ifdef _WIN32
        HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
        COORD coord = {0, 0};
        DWORD count;
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(hStdOut, &csbi);
        FillConsoleOutputCharacter(hStdOut, ' ',
                                   csbi.dwSize.X * csbi.dwSize.Y,
                                   coord, &count);
        SetConsoleCursorPosition(hStdOut, coord);
    #else
        cout << "\f";
    #endif
}

void initialize(){

    room[0][0] = Room(false,SOUTH,             0,       0,       0); 
    room[0][1] = Room(false,NORTH+ SOUTH,      0,       0,       0);
    room[0][2] = Room(false,NORTH+ EAST,       0,       0,       0);
    room[0][3] = Room(false,SOUTH+ EAST,       0,       0,       0);
    room[0][4] = Room(false,NORTH+ EAST,       0,       0,       0);
    
    room[1][0] = Room(false,SOUTH+ EAST,       0,       0,       0);
    room[1][1] = Room(false,NORTH+ EAST,       0,       0,       0);
    room[1][2] = Room(false,WEST + SOUTH,      0,       0,       0);
    room[1][3] = Room(false,NORTH+ WEST,       0,       0,       0);
    room[1][4] = Room(false,WEST + EAST,       0,       0,       0);
    
    room[2][0] = Room(false,WEST + EAST,       0,       0,       0);
    room[2][1] = Room(false,WEST + SOUTH,      0,       0,       0);
    room[2][2] = Room(false,NORTH+ SOUTH,      0,       0,       0);
    room[2][3] = Room(false,NORTH+ SOUTH,      0,       0,       0);
    room[2][4] = Room(false,NORTH+ WEST,       0,       0,       0);
    
    room[3][0] = Room(false,WEST + SOUTH,      0,       0,       0);
    room[3][1] = Room(false,NORTH+ SOUTH,      0,       0,       0);
    room[3][2] = Room(false,NORTH+ SOUTH,      0,       0,       0);
    room[3][3] = Room(false,NORTH+ SOUTH,      0,       0,       0);
    room[3][4] = Room(false,NORTH+ EAST,       0,       0,       0);
    
    room[4][0] = Room(true,SOUTH,              0,       0,       0);
    room[4][1] = Room(false,NORTH+SOUTH,       0,       0,       0);
    room[4][2] = Room(false,NORTH+SOUTH,       0,       0,       0);
    room[4][3] = Room(false,NORTH+SOUTH,       0,       0,       0);
    room[4][4] = Room(false,NORTH+WEST,        0,       0,       0); 
     
     
    currentX = 0;
    currentY = 0;
    PROMPT = "\nAction: ";

    room[currentX][currentY].enterRoom();
    cout <<"Hello, try \"help\" to get started.\n";
    
}
